module Spacetime

using Color
using Compose

import Base: show



function setindex{T}(a::Array{T}, x)
    b = copy(a)
    b[] = x
    b
end
function setindex{T}(a::Array{T}, x, i0::Real)
    b = copy(a)
    b[i0] = x
    b
end
function setindex{T}(a::Array{T}, x, i0::Real, i1::Real)
    b = copy(a)
    b[i0,i1] = x
    b
end



const MAXRANK = 10
function make_bndcounts()
    cts = Array(Int,MAXRANK+2,MAXRANK+2)
    cts[1,1] = 1
    for br in 0:MAXRANK
        cts[1,br+2] = 0
    end
    for er in 0:MAXRANK
        cts[er+2,1] = 1
        for br in 0:MAXRANK
            cts[er+2,br+2] = cts[er+1,br+1] + cts[er+1,br+2]
        end
    end
    cts
end
const BNDCOUNTS = make_bndcounts()
function bndcount(eltrank::Int, bndrank::Int)
    @assert -1 <= bndrank <= eltrank <= MAXRANK
    BNDCOUNTS[eltrank+2,bndrank+2]
end



type Element
    id::Int
    rank::Int
    pos::Array{Float64,1}
    active::Bool
    bnds::Array{Element,1}
    rbnds::Array{Element,1}
    function Element(id::Int, maxrank::Int, rank::Int, pos::Array{Float64,1},
                     bnds::Array{Element,1})
        @assert rank>=0 && rank<=maxrank
        @assert length(pos) == maxrank
        @assert length(bnds) == (rank==0 ? 0 : rank+1)
        @assert length(bnds) == (rank==0 ? 0 : bndcount(rank, rank-1))
        for elt in bnds
            @assert elt.rank == rank-1
        end
        for i in 1:length(bnds)
            for j in i+1:length(bnds)
                @assert bnds[i] != bnds[j]
            end
        end
        elt = new(id, rank, copy(pos), false, bnds, Array(Element,0))
        for bnd in bnds
            push!(bnd.rbnds, elt)
        end
        elt
    end
end

type Manifold
    maxrank::Int
    maxid::Int
    eltss::Array{Array{Element,1},1}
    function Manifold(maxrank::Int)
        @assert maxrank>=0
        mf = new(maxrank, 0, Array(Array{Element,1}, maxrank+1))
        for rank in 0:maxrank
            mf.eltss[rank+1] = Array(Element,0)
        end
        mf
    end
end

function Element(mf::Manifold, rank::Int, pos::Array{Float64,1},
                 bnds::Array{Element,1})
    if !isempty(bnds)
        @assert rank>0
        bndrank = bnds[1].rank
        @assert bndrank>=0 && bndrank<rank
        for bnd in bnds @assert bnd.rank == bndrank end
        @assert length(bnds) == bndcount(rank, bndrank)
        while bndrank<rank-1
            elts = bnds
            bnds = Element[]
            bndset = Set{Element}()
            for elt in elts
                for rbnd in elt.rbnds
                    if rbnd ∉ bndset
                        if all(bnd->bnd ∈ elts, rbnd.bnds)
                            push!(bnds, rbnd)
                            push!(bndset, rbnd)
                        end
                    end
                end
            end
            bndrank += 1
            @assert length(bnds) == bndcount(rank, bndrank)
        end
        @assert bndrank == rank-1
    end
    mf.maxid += 1
    elt = Element(mf.maxid, mf.maxrank, rank, pos, bnds)
    push!(mf.eltss[rank+1], elt)
    elt
end

function Element(mf::Manifold, rank::Int, bnds::Array{Element,1})
    Element(mf, rank, meanpos(bnds), bnds)
end



function meanpos(elts)
    pos = Float64[]
    count = 0.0
    for elt in elts
        while length(pos) < length(elt.pos) push!(pos, 0.0) end
        pos += elt.pos
        count += 1
    end
    pos /= count
    pos
end



function boundaries(elt::Element, level::Int=1)
    boundaries([elt], level)
end
function boundaries(elts::Array{Element,1}, level::Int=1)
    @assert level>=0
    if !isempty(elts) @assert level<=elts[1].rank end
    for elt in elts
        @assert elt.rank == elts[1].rank
    end
    while level>0
        level -= 1
        bnds = Element[]
        bndset = Set{Element}()
        for elt in elts
            for bnd in elt.bnds
                if bnd ∉ bndset
                    push!(bnds, bnd)
                    push!(bndset, bnd)
                end
            end
        end
        elts = bnds
    end
    elts
end

function rboundaries(elt::Element, level::Int=1)
    rboundaries([elt], level)
end
function rboundaries(elts::Array{Element,1}, level::Int=1)
    @assert level>=0
    # if !isempty(elts) @assert elts[1].rank+level<=maxrank end
    for elt in elts
        @assert elt.rank == elts[1].rank
    end
    while level>0
        level -= 1
        rbnds = Element[]
        rbndset = Set{Element}()
        for elt in elts
            for rbnd in elt.rbnds
                if rbnd ∉ rbndset
                    push!(rbnds, rbnd)
                    push!(rbndset, rbnd)
                end
            end
        end
        elts = rbnds
    end
    elts
end



function show(io::IO, elt::Element)
    print(io,
          "{",
          "id=$(elt.id) ",
          "rank=$(elt.rank) ",
          "pos=$(elt.pos) ",
          "active=$(elt.active) ",
          "bnds=$([bnd.id for bnd in elt.bnds]) ",
          "rbnds=$([rbnd.id for rbnd in elt.rbnds])}")
end

function show(io::IO, mf::Manifold)
    println(io, "Manifold:")
    for rank in 0:mf.maxrank
        println(io, "    rank $rank:")
        for elt in mf.eltss[rank+1]
            println(io, "        ", elt)
        end
    end
end

function mkplot(elt::Element)
    maxcolor = 100
    rank_colormaps = (colormap("Reds", maxcolor+1),
                      colormap("Greens", maxcolor+1),
                      colormap("Blues", maxcolor+1),
                      colormap("Purples", maxcolor+1),
                      colormap("Oranges", maxcolor+1))
    elts = boundaries(elt, elt.rank)
    ctr = meanpos(elts)
    contract(pos) = pos - 0.1 * (pos-ctr)
    pos1(pos) = length(pos) >= 1 ? pos[1] : 0.0
    pos2(pos) = length(pos) >= 2 ? pos[2] : 0.0
    pos3(pos) = length(pos) >= 3 ? pos[3] : 0.0
    xy(pos) = (pos1(pos), pos2(pos))
    z(pos) = pos3(pos)
    cmin = 25; cmax = 75
    zmin = 0.0; zmax = 1.0
    colval(pos) = clamp(int(cmin + (cmax-cmin) * (z(pos)-zmin) / (zmax-zmin)),
                        0, maxcolor)
    col(pos) = rank_colormaps[elt.rank+1][colval(pos)+1]
    ctxts = {}
    if elt.rank==0
        for i in 1:length(elts)
            push!(ctxts, compose(context(),
                                 circle(xy(contract(elts[i].pos))..., 0.03),
                                 fill(col(ctr))))
        end
    else
        for i in 1:length(elts)
            for j in i+1:length(elts)
                push!(ctxts, compose(context(),
                                     line([xy(contract(elts[i].pos)),
                                           xy(contract(elts[j].pos))]),
                                     stroke(col(ctr))))
            end
        end
    end
    compose(context(), ctxts)
end

function mkplot(mf::Manifold)
    ctxts = {}
    for rank in 0:mf.maxrank
        for elt in mf.eltss[rank+1]
            push!(ctxts, mkplot(elt))
        end
    end
    compose(context(), ctxts)
end



# :isometric, :causal
const edgetypes = [:isometric,  # vertex
                   :isometric,  # edge
                   :isometric,  # face
                   :isometric]  # volume

function euclid(pos1::Array{Float64,1}, pos2::Array{Float64,1})
    @assert length(pos1) == length(pos2)
    sum(pos1 .* pos2)
end
function euclid2(pos::Array{Float64,1})
    euclid(pos, pos)
end
function minkowski(pos1::Array{Float64,1}, pos2::Array{Float64,1}, dim::Integer)
    @assert length(pos1) == length(pos2)
    diag(d) = d==dim ? -1 : +1
    sum(d->diag(d) * pos1[d] * pos2[d], 1:length(pos1))
end
function minkowski2(pos::Array{Float64,1}, dim::Integer)
    minkowski(pos, pos, dim)
end



function make_vertex(mf::Manifold)
    pos = fill(0.0, mf.maxrank)
    elt0 = Element(mf, 0, pos, Element[])
    elt0
end

function make_edge_over_vertex(mf::Manifold, elt0::Element)
    @assert elt0.rank == 0
    @assert elt0.active
    pos = copy(elt0.pos); pos[1]+=1
    @assert isapprox(euclid2(pos - elt0.pos), 1)
    elt0new = Element(mf, 0, pos, Element[])
    elt1new = Element(mf, 1, [elt0, elt0new])
    elt0.active = false
    elt0new.active = true
    [elt0new], elt1new
end

function make_face_over_edge(mf::Manifold, elt1::Element)
    @assert elt1.rank == 1
    @assert elt1.active
    elt0s = boundaries(elt1)
    @assert all(elt->elt.active, elt0s)
    if edgetypes[2] == :isometric
        pos = copy(elt1.pos); pos[2]+=sqrt(3/4)
        @assert all(elt->isapprox(euclid2(pos - elt.pos), 1),
                    boundaries(elt1))
    elseif edgetypes[2] == :causal
        # pos1 = elt1.bnds[1].pos
        # pos2 = elt1.bnds[2].pos
        # pos = (pos1 + pos2) / 2
        # 
        # TODO length, normalize, project, basis, ...
        # 
        # d = pos - pos1
        # d2 = minkowski2(d, mf.maxrank)
        # @assert d2 > 0.0
        # d /= sqrt(abs(d2))
        # n = fill(0.0, mf.maxrank); n[2] = 1.0
        # n2 = minkowski2(n, mf.maxrank)
        # @assert n2 < 0.0
        # n /= sqrt(abs(n))
        # n += minkowski(d, n, mf.maxrank)
        # @assert minkowski2(n, mf.maxrank) < 0.0
        # pos += n * sqrt(d2)
        # @assert isapprox(minkowski2(pos - pos1, 2), 0)
        # @assert isapprox(minkowski2(pos - pos2, 2), 0)
    else
        @assert false
    end
    elt0new = Element(mf, 0, pos, Element[])
    elt1news = [Element(mf, 1, [elt, elt0new]) for elt in elt0s]
    elt2new = Element(mf, 2, [elt0s, elt0new])
    elt1.active = false
    elt0new.active = true
    for elt in elt1news elt.active=true end
    [elt0new], elt1news, elt2new
end

function make_face_over_vertex(mf::Manifold, elt0::Element)
    @assert elt0.rank == 0
    @assert elt0.active
    # Vertex needs 2 active edges
    elt1s = filter(elt->elt.active, rboundaries(elt0))
    @assert length(elt1s) == 2
    @assert all(elt->elt.active, boundaries(elt1s))
    elt0s = setdiff(boundaries(elt1s), [elt0])
    @assert length(elt0s) == 2
    elt1new = Element(mf, 1, elt0s)
    elt2new = Element(mf, 2, [elt0, elt0s])
    elt0.active = false
    for elt in elt1s elt.active=false end
    elt1new.active = true
    Element[], [elt1new], elt2new
end

function make_volume_over_face(mf::Manifold, elt2::Element)
    @assert elt2.rank == 2
    @assert elt2.active
    elt1s = boundaries(elt2)
    @assert all(elt->elt.active, elt1s)
    elt0s = boundaries(elt2,2)
    @assert all(elt->elt.active, elt0s)
    if edgetypes[3] == :isometric
        pos = copy(elt2.pos); pos[3]+=sqrt(2/3)
        @assert all(elt->isapprox(euclid2(pos - elt.pos), 1),
                    boundaries(elt2,2))
    elseif edgetypes[3] == :causal
        # pos1 = elt2.bnds[1].pos
        # pos2 = elt2.bnds[2].pos
        # pos3 = elt2.bnds[3].pos
        # pos = (pos1 + pos2) / 2
        # d = pos - pos1
        # d2 = minkowski2(d, mf.maxrank)
        # d /= sqrt(d)
        # n = pos - pos3
        # n2 = minkowski2(n, mf.maxrank)
        # n /= sqrt(n)
        # n -= minkowski(d, n, mf.maxrank)
        # pos += n * 
        # 
        # pos = (pos1 + pos2 + pos3) / 3
        # d = pos - pos1
        # d2 = minkowski2(d)
        # @assert isapprox(minkowski2(pos-pos1), d2)
        # @assert isapprox(minkowski2(pos-pos2), d2)
        # @assert isapprox(minkowski2(pos-pos3), d2)
    else
        @assert false
    end
    @assert all(elt->isapprox(sum((pos .- elt.pos).^2), 1), boundaries(elt2,2))
    elt0new = Element(mf, 0, pos, Element[])
    elt1news = [Element(mf, 1, [elt, elt0new]) for elt in elt0s]
    elt2news = [Element(mf, 2, [elt0a, elt0b, elt0new])
                for (elt0a,elt0b) in combinations(elt0s, 2)]
    elt3new = Element(mf, 3, [elt0s, elt0new])
    elt2.active = false
    elt0new.active = true
    for elt in elt1news elt.active=true end
    for elt in elt2news elt.active=true end
    [elt0new], elt1news, elt2news, elt3new
end

function make_volume_over_edge(mf::Manifold, elt1::Element)
    @assert elt1.rank == 1
    @assert elt1.active
    # Edge needs 2 active faces
    elt2s = filter(elt->elt.active, rboundaries(elt1))
    @assert length(elt2s) == 2
    @assert all(elt->elt.active, boundaries(elt2s))
    @assert all(elt->elt.active, boundaries(elt2s, 2))
    # elt1s = setdiff(boundaries(elt2s), [elt1])
    # @assert length(elt1s) == 4
    elt0s = setdiff(boundaries(elt2s, 2), boundaries(elt1))
    @assert length(elt0s) == 2
    elt1new = Element(mf, 1, elt0s)
    elt2news = [Element(mf, 2, [elt0s, elt]) for elt in boundaries(elt1)]
    elt3new = Element(mf, 3, [elt0s, boundaries(elt1)])
    elt1.active = false
    for elt in elt2s elt.active=false end
    elt1new.active = true
    for elt in elt2news elt.active=true end
    Element[], [elt1new], elt2news, elt3new
end

function make_volume_over_vertex(mf::Manifold, elt0::Element)
    @assert elt0.rank == 0
    @assert elt0.active
    # Vertex needs 3 active edges and faces
    elt1s = filter(elt->elt.active, rboundaries(elt0))
    @assert length(elt1s) == 3
    elt2s = filter(elt->elt.active, rboundaries(elt0, 2))
    @assert length(elt2s) == 3
    @assert all(elt->elt.active, boundaries(elt2s))
    @assert all(elt->elt.active, boundaries(elt2s, 2))
    elt0s = setdiff(boundaries(elt2s, 2), [elt0])
    @assert length(elt0s) == 3
    elt2new = Element(mf, 2, elt0s)
    elt3new = Element(mf, 2, [elt0, elt0s])
    elt0.active = false
    for elt in elt1s elt.active = false end
    for elt in elt2s elt.active = false end
    elt2new.active = true
    Element[], Element[], [elt2new], elt3new
end



function make_first_vertex(mf::Manifold)
    elt0new = make_vertex(mf)
    elt0new
end

function make_first_edge(mf::Manifold, elt0::Element, npoints::Integer)
    @assert !elt0.active
    elt0.active = true
    elt1news = Element[]
    for i in 1:npoints
        elt0news, elt1new = make_edge_over_vertex(mf, elt0)
        @assert length(elt0news) == 1
        elt0 = elt0news[1]
        push!(elt1news, elt1new)
    end
    @assert elt0.active
    elt0.active = false
    elt1news
end

function make_first_face(mf::Manifold, elt1s::Array{Element,1})
    @assert all(elt->!elt.active, elt1s)
    for elt in elt1s elt.active=true end
    elt0s = boundaries(elt1s)
    @assert all(elt->!elt.active, elt0s)
    for elt in elt0s elt.active=true end
    elt2news = Element[]
    while !isempty(elt1s)
        #
        elt0todos1 = Element[]
        elt1todos1 = Element[]
        for elt1 in elt1s
            elt0news, elt1news, elt2new = make_face_over_edge(mf, elt1)
            append!(elt0todos1, elt0news)
            append!(elt1todos1, elt1news)
            push!(elt2news, elt2new)
        end
        @assert all(elt->!elt.active, elt1s)
        #
        elt0todos0 = Element[]
        elt1todos0 = Element[]
        for elt0 in elt0s
            cnt = count(elt->elt.active, elt0.rbnds)
            @assert cnt <= 2
            if cnt < 2
                # skip boundaries
                elt0.active = false
                @assert count(elt->elt.active, rboundaries(elt0)) == 1
                for elt in rboundaries(elt0) elt.active=false end
                continue
            end
            elt0news, elt1news, elt2new = make_face_over_vertex(mf, elt0)
            append!(elt0todos0, elt0news)
            append!(elt1todos0, elt1news)
            push!(elt2news, elt2new)
        end
        @assert all(elt->!elt.active, elt0s)
        #
        @assert all(elt->!elt.active, elt1todos1)
        @assert all(elt->!elt.active, elt0todos0)
        #
        @assert all(elt->elt.active, elt0todos1)
        @assert all(elt->elt.active, elt1todos0)
        elt0s = elt0todos1
        elt1s = elt1todos0
    end
    @assert length(elt0s) == 1
    @assert elt0s[1].active
    elt0s[1].active = false
    elt2news
end

function make_first_volume(mf::Manifold, elt2s::Array{Element,1})
    @assert all(elt->!elt.active, elt2s)
    for elt in elt2s elt.active=true end
    elt1s = boundaries(elt2s)
    @assert all(elt->!elt.active, elt1s)
    for elt in elt1s elt.active=true end
    elt0s = boundaries(elt1s)
    @assert all(elt->!elt.active, elt0s)
    for elt in elt0s elt.active=true end
    elt3news = Element[]
    begin
        # 1. make_volume_over_face
        elt0todos2 = Element[]
        elt1todos2 = Element[]
        elt2todos2 = Element[]
        for elt2 in elt2s
            elt0news, elt1news, elt2news, elt3new =
            make_volume_over_face(mf, elt2)
            append!(elt0todos2, elt0news)
            append!(elt1todos2, elt1news)
            append!(elt2todos2, elt2news)
            push!(elt3news, elt3new)
        end
        @assert all(elt->!elt.active, elt2s)
        # 2. make_volume_over_edge
        elt0todos1 = Element[]
        elt1todos1 = Element[]
        elt2todos1 = Element[]
        for elt1 in elt1s
            cnt = count(elt->elt.active, elt1.rbnds)
            @assert cnt <= 2
            if cnt < 2
                # skip boundaries
                elt1.active = false
                @assert count(elt->elt.active, rboundaries(elt1)) == 1
                for elt in rboundaries(elt1) elt.active=false end
                continue
            end
            elt0news, elt1news, elt2news, elt3new =
            make_volume_over_edge(mf, elt1)
            append!(elt0todos1, elt0news)
            append!(elt1todos1, elt1news)
            append!(elt2todos1, elt2news)
            push!(elt3news, elt3new)
        end
        @assert all(elt->!elt.active, elt1s)
        # 3. make_volume_over_edge (multiple times, in the valleys)
        elt0todos0 = Element[]
        elt1todos0 = Element[]
        elt2todos0 = Element[]
        for elt0 in elt0s
            elt1acts = filter(elt->elt.active, rboundaries(elt0))
            elt2acts = filter(elt->elt.active, rboundaries(elt0, 2))
            # TODO: also need to do this near boundaries
            @assert length(elt1acts) <= 6 && length(elt2acts) <= 6
            if length(elt1acts) < 6 && length(elt2acts) < 6
                # skip boundaries
                elt0.active = false
                # TODO: check intersection between those two and elt1s/elt2s
                # @assert all(elt->!elt.active, rboundaries(elt0))
                # @assert all(elt->!elt.active, rboundaries(elt0, 2))
                continue
            end
            @assert length(elt1acts) == 6 && length(elt2acts) == 6
            while !isempty(elt2acts)
                # pick 2 neighbouring faces
                elt2a = elt2acts[1]
                elt2bs = setdiff(rboundaries(boundaries(elt2a)) ∩ elt2acts,
                                 [elt2a])
                @assert length(elt2bs) >= 1
                elt2b = elt2bs[1]
                elt2acts = setdiff(elt2acts, [elt2a, elt2b])
                elt0ab = boundaries(elt2a,2) ∩ boundaries(elt2b,2)
                elt0cd = symdiff(boundaries(elt2a,2), boundaries(elt2b,2))
                elt1new = Element(mf, 1, elt0cd)
                elt2news = [Element(mf, 2, [elt0cd, elt]) for elt in elt0ab]
                elt3new = Element(mf, 3, [elt0ab, elt0cd])
                # push!(elt1todos0, elt1new)
                # append!(elt2todos0, elt2news)
                push!(elt3news, elt3new)
            end
        end
    end
    for elts in mf.eltss, elt in elts
        elt.active = false
    end
    # 3. 3 * make_volume_over_edge (in the valleys)
    # 4. make_volume_over_vertex (in the valleys)
end



function make_grid()
    maxrank = 3
    npoints = 4
    mf = Manifold(maxrank)
    @assert all(elts->all(elt->!elt.active, elts), mf.eltss)
    elt0 = make_first_vertex(mf)
    @assert all(elts->all(elt->!elt.active, elts), mf.eltss)
    elt1s = make_first_edge(mf, elt0, npoints)
    @assert all(elts->all(elt->!elt.active, elts), mf.eltss)
    elt2s = make_first_face(mf, elt1s)
    @assert all(elts->all(elt->!elt.active, elts), mf.eltss)
    elt3s = make_first_volume(mf, elt2s)
    @assert all(elts->all(elt->!elt.active, elts), mf.eltss)
    mf
end



mf = make_grid()
print(mf)
draw(SVG("manifold.svg", 30cm, 30cm),
     compose(context(0, 1, 1, -1),
             (context(units=UnitBox(-1, -1, 11, 11)), mkplot(mf))))

end
